import { connection } from "../src/connection";
import { Wedding } from "../src/entities";
import { Expense } from "../src/entities";
import { WeddingPlannerDAO } from "../src/daos/wedding-planner-dao";
import { WeddingPlannerDaoPostgres } from "../src/daos/wedding-planner-dao-postgres";

const weddingplannerDAO:WeddingPlannerDAO = new WeddingPlannerDaoPostgres();

// 1 POST /weddings 
// create a wedding
test("Create a Wedding", async()=>{
    const testWedding:Wedding = new Wedding(0,"2021-01-01","Maryland","First Wedding", 1000);
    const result:Wedding = await weddingplannerDAO.createWedding(testWedding);
    expect(result.wedding_id).not.toBe(0);
})

// 2 GET /weddings 
// get all weddings
test("Get all Weddings", async()=>{
    const wedding1:Wedding = new Wedding(0,"2021-01-02","New York","Second Wedding", 2000);
    const wedding2:Wedding = new Wedding(0,"2021-01-03","Virginia","Third Wedding", 3000);
    const wedding3:Wedding = new Wedding(0,"2021-01-04","Utah","Fourth Wedding", 4000);

    await weddingplannerDAO.createWedding(wedding1)
    await weddingplannerDAO.createWedding(wedding2)
    await weddingplannerDAO.createWedding(wedding3)

    const weddings:Wedding[] = await weddingplannerDAO.getAllWeddings();

    expect(weddings.length).toBeGreaterThanOrEqual(3);
})

// 3 GET /weddings/:id
// get wedding by id

test("Get Wedding by ID", async()=>{
    let wedding:Wedding = new Wedding(0,"2021-01-05","Florida","Fifth Wedding", 5000);
    wedding = await weddingplannerDAO.createWedding(wedding);

    let retrievedWedding:Wedding = await weddingplannerDAO.getWeddingByID(wedding.wedding_id);

    expect(retrievedWedding.wedding_date).toBe(wedding.wedding_date);

})

// 4 -PUT /weddings/id 
// update wedding with id of 

test("Update Wedding", async()=>{
    let wedding:Wedding = new Wedding(0,"2021-01-06","Georgia","Sixth Wedding", 6000);
    wedding = await weddingplannerDAO.createWedding(wedding);

    wedding.wedding_date = "2021-01-06";

    wedding = await weddingplannerDAO.updateWedding(wedding);

    expect(wedding.wedding_date).toBe("2021-01-06")
})

// 5 DELETE /weddings/:id
// delete wedding with id of 

test("Delete Wedding", async()=>{
    let wedding:Wedding = new Wedding(0,"2021-01-07","West Virginia","Seventh Wedding", 7000);
    wedding = await weddingplannerDAO.createWedding(wedding);

    const result:boolean = await weddingplannerDAO.deleteWeddingByID(wedding.wedding_id);

    expect(result).toBeTruthy();
})


//////////////////////////////// Expense

// 6 POST /expenses
// create an expense

test("Create an Expense", async()=>{
    const testExpense:Expense = new Expense(0,"wedding pictures",100,"-",1); // passing in wedding id of 1, so there how to be an wedding id of 1 already in the wedding table
    const result:Expense = await weddingplannerDAO.createExpense(testExpense);
    expect(result.expense_id).not.toBe(0);
})

// 7 GET /expenses
// get all expenses

test("Get all Expenses", async()=>{
    const expense1:Expense = new Expense(0,"tables",200,"-",1);
    const expense2:Expense = new Expense(0,"food",300,"-",1);
    const expense3:Expense = new Expense(0,"video",400,"-",1);

    await weddingplannerDAO.createExpense(expense1)
    await weddingplannerDAO.createExpense(expense2)
    await weddingplannerDAO.createExpense(expense3)

    const expenses:Expense[] = await weddingplannerDAO.getAllExpenses();

    expect(expenses.length).toBeGreaterThanOrEqual(3);
})


// 8 GET /expenses/:id 
// get expense by id

test("Get Expense by ID", async()=>{
    let expense:Expense = new Expense(0,"chairs", 600,"-", 1);
    expense = await weddingplannerDAO.createExpense(expense);

    let retrievedExpense:Expense = await weddingplannerDAO.getExpense(expense.expense_id);

    expect(retrievedExpense.amount).toBe(expense.amount);

})

// 9 PUT /expenses/:id 
// update expense with the id of

test("Update Expense", async()=>{

    // create Wedding
    const testWedding:Wedding= new Wedding(0,"2021-01-08","Texas","Eigth Wedding", 8000);
    const wResult:Wedding = await weddingplannerDAO.createWedding(testWedding);

    // create Expense
    let expense1:Expense = new Expense(0,"venue",700,"-",wResult.wedding_id);
    let retrievedExpense:Expense = await weddingplannerDAO.createExpense(expense1);

    let expense2:Expense = new Expense(retrievedExpense.expense_id,"new venue", 800,"-", 1); // new reason, new budget, new wedding id

    let tempExpense:Expense = await weddingplannerDAO.updateExpense(expense2);

    expect(tempExpense.expense_id).toEqual(expense2.expense_id);
    expect(tempExpense.reason).toEqual(expense2.reason);
    expect(tempExpense.amount).toEqual(expense2.amount);
    expect(tempExpense.wedding_id_fk).toEqual(expense2.wedding_id_fk)
})


// 10 DELETE /expenses/:id 
// delete expense with the id of 

test("Delete Expense", async()=>{

    const testWedding:Wedding = new Wedding(0,"2021-01-09","Delaware","Ninth Wedding", 9000);
    const wResult:Wedding = await weddingplannerDAO.createWedding(testWedding);

    // create Account with balance 0
    let expense1:Expense = new Expense(0,"transportation",800,"-",wResult.wedding_id);
    let retrievedExpense:Expense = await weddingplannerDAO.createExpense(expense1);

    const result:boolean = await weddingplannerDAO.deleteExpenseByID(retrievedExpense.expense_id);

    expect(result).toBeTruthy();


})

// 11 GET /weddings/:id/expenses 
// get expenses by wedding id

test("Get Expenses by Wedding ID", async()=>{

    const testWedding:Wedding= new Wedding(0,"2021-01-10","Louisiana","Tenth Wedding", 10000);
    const wResult:Wedding = await weddingplannerDAO.createWedding(testWedding);

    let expense1:Expense = new Expense(0,"camera",900,"-",wResult.wedding_id);
    let expense2:Expense = new Expense(0,"Tech",1000,"-",wResult.wedding_id);
    let expense3:Expense = new Expense(0,"Cleaning",1100,"-",wResult.wedding_id);

    await weddingplannerDAO.createExpense(expense1)
    await weddingplannerDAO.createExpense(expense2)
    await weddingplannerDAO.createExpense(expense3)

    const expenses:Expense[] = await weddingplannerDAO.getExpenseByID(wResult.wedding_id);

    expect(expenses.length).toEqual(3)

})




afterAll(async()=>{
    connection.end();
})