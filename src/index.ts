import express from 'express';
import { Wedding } from './entities';
import { Expense } from "./entities";
import { MissingResourceError } from './errors';
import WeddingPlannerService from './services/wedding-planner-services';
import { WeddingPlannerServicesImpl } from './services/wedding-planner-services-impl';
import cors from 'cors';

const app = express();

app.use(express.json());
app.use(cors())

const weddingplannerService:WeddingPlannerService = new WeddingPlannerServicesImpl();

// 1) Get Weddings

app.get("/weddings", async(req,res)=>{

    try 
    {
        const weddings: Wedding[] = await weddingplannerService.retrieveAllWeddings();
        res.status(200);
        res.send(weddings);    
    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }

})

// 2) Get Wedding with specific ID

app.get("/weddings/:id", async(req,res)=>
{
    try 
    {
        const weddingID = Number(req.params.id);
        const wedding:Wedding = await weddingplannerService.retrieveWeddingByID(weddingID);
        res.send(wedding);

    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            //res.status(404);
            res.send(error);
        }    
    }
})

// 3) get all expenses for wedding id(ex: 5)
app.get("/weddings/:id/expenses", async(req,res)=>{

    try 
    {
        const weddingID = Number(req.params.id);
        const result:Expense[] = await weddingplannerService.getExpensesByID(weddingID);
        res.send(result); // returns true for deleted
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

// 4) Create a wedding
app.post("/weddings",async (req,res)=>{
    let wedding:Wedding = req.body;
    wedding = await weddingplannerService.registerWedding(wedding);
    res.status(201);
    res.send(wedding);
})

// 5) delete a wedding

app.delete("/weddings/:id", async(req,res)=>{
    try 
    {
        const weddingID = Number(req.params.id);
        const wedding:boolean = await weddingplannerService.removeWeddingByID(weddingID);
        res.status(205);
        res.send(wedding); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

// 6) update wedding of id

app.put("/weddings/:id", async(req,res)=>{ // may have to add id in the body also 
    // client with id 
    try 
    {
        let weddingID = Number(req.params.id);
        let wedding_body = req.body;
        let wedding:Wedding = await weddingplannerService.retrieveWeddingByID(weddingID); // get client with ID requested
        wedding.wedding_date = wedding_body.wedding_date; // update first name
        wedding.wedding_location = wedding_body.wedding_location;
        wedding.wedding_name = wedding_body.wedding_name;
        wedding.wedding_budget = wedding_body.wedding_budget;

        wedding = await weddingplannerService.modifyWedding(wedding);
        res.send(wedding);
    } 
    catch (error) 
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }
})


// 7) Get all expenses

app.get("/expenses",async(req,res)=>
{
    try 
    {
        const expenses: Expense[] = await weddingplannerService.retrieveAllExpenses();
        res.status(200);
        res.send(expenses);    
    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }
})

// 8) get expense by id

app.get("/expenses/:id", async(req,res)=>{
    try 
    {
        const expenseID = Number(req.params.id);
        const result:Expense = await weddingplannerService.getExpenses(expenseID);
        res.send(result); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

// 9) create expense
app.post("/expenses",async (req,res)=>{
    try{

        const weddingID = req.body.wedding_id_fk
        const tempWedding:Wedding = await weddingplannerService.retrieveWeddingByID(weddingID)

        // will come down here only if above doesnt throw error. Wont throw error if expence fk is found
        let expense:Expense = req.body;
        expense = await weddingplannerService.registerExpense(expense);
        res.status(201);
        res.send(expense);
    }
    catch(error)
    {
        res.status(404);
        res.send(error)
    }
})

// 10) update expense with id of 
app.put("/expenses/:id", async(req,res)=>{
    try
    {
        const expenseID = Number(req.params.id);
        const tempExpense:Expense = await weddingplannerService.getExpenses(expenseID)

        const tempReason = req.body.reason;
        const tempAmount = req.body.amount;
        const weddingID = req.body.wedding_id_fk;
        //
        const tempPhoto = req.body.photo;
    
        const expense:Expense = new Expense(tempExpense.expense_id, tempReason,tempAmount,tempPhoto, weddingID );
        const result:Expense = await weddingplannerService.updateExpense(expense);    

        res.send(result);
    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

// 11) delete expense

app.delete("/expenses/:id", async(req,res)=>{
    try 
    {
        const expenseID = Number(req.params.id);
        const expense:boolean = await weddingplannerService.removeExpenseByID(expenseID);
       //res.status(205);
        res.send(expense); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

app.listen(3000,()=>{console.log("Application started")});