import { Wedding } from "../entities";
import { Expense } from "../entities";
import { WeddingPlannerDAO } from "./wedding-planner-dao";
import { connection } from "../connection";
import { MissingResourceError } from "../errors";

export class WeddingPlannerDaoPostgres implements WeddingPlannerDAO
{

    async getAllWeddings(): Promise<Wedding[]>
    {
        const sql:string = "select * from wedding";
        const result = await connection.query(sql);
        const clients:Wedding[] = [];
        for(const row of result.rows)
        {
            const wedding:Wedding = new Wedding(row.wedding_id,row.wedding_date,row.wedding_location,row.wedding_name,row.wedding_budget);
            clients.push(wedding);
        }
        if(clients.length == 0)
        {
            throw new MissingResourceError('There are no weddings')
        }
        return clients;
    }
    async getWeddingByID(wedding_id: number): Promise<Wedding>
    {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [wedding_id]
        const result = await connection.query(sql,values);
        const row = result.rows[0];
        if(result.rowCount=== 0)
        {
            throw new MissingResourceError(`The Wedding with id ${wedding_id} does not exist`);
        }
        const wedding:Wedding = new Wedding(row.wedding_id,row.wedding_date,row.wedding_location,row.wedding_name,row.wedding_budget);
        return wedding;
    }

    async createWedding(wedding: Wedding): Promise<Wedding>
    {
        const sql:string = "insert into wedding(wedding_date,wedding_location,wedding_name,wedding_budget) values ($1,$2,$3,$4) returning wedding_id"
        const values = [wedding.wedding_date,wedding.wedding_location,wedding.wedding_name,wedding.wedding_budget];
        const result = await connection.query(sql,values);
        wedding.wedding_id = result.rows[0].wedding_id; // will get the latest id of the newest record that we are creating;
        return wedding;
    }

    async updateWedding(wedding: Wedding): Promise<Wedding>
    {
        const sql:string = 'update wedding set wedding_date=$1, wedding_location=$2, wedding_name=$3,wedding_budget=$4 where wedding_id=$5';
        const values = [wedding.wedding_date, wedding.wedding_location, wedding.wedding_name, wedding.wedding_budget, wedding.wedding_id];
        const result = await connection.query(sql,values)
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Client with id ${wedding.wedding_id} does not exist`);
        }
        return wedding;
    }
    async deleteWeddingByID(wedding_id: number): Promise<boolean>
    {
        const sql: string = 'delete from wedding where wedding_id =$1'
        const values = [wedding_id];
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Wedding with id ${wedding_id} does not exist`);
        }
        return true;
    }
    async createExpense(expense: Expense): Promise<Expense>
    {
        const sql:string = "insert into expense(reason,amount, photo, wedding_id_fk) values ($1,$2,$3,$4) returning expense_id"
        const values = [expense.reason, expense.amount, expense.photo,expense.wedding_id_fk];
        const result = await connection.query(sql,values);
        expense.expense_id = result.rows[0].expense_id; // will get the latest id of the newest record that we are creating;
        return expense;
    }
    async getExpenseByID(wedding_id_fk: number): Promise<Expense[]> 
    {
        const sql:string = 'select * from expense where wedding_id_fk = $1'
        const values = [wedding_id_fk];
        const result = await connection.query(sql,values);
        let expenses:Expense[] = [];
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`No Expense for Wedding with  id of ${wedding_id_fk}`);
        }
       for(const row of result.rows)
       {
           const expense:Expense = new Expense(row.expense_id, row.reason, row.amount, row.photo, row.wedding_id_fk);
           expenses.push(expense);
       }

        return expenses;
    }
    async getExpense(expense_id: number): Promise<Expense>
    {
        const sql:string = 'select * from expense where expense_id = $1'
        const values = [expense_id];
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`No Expense with  id of ${expense_id}`);
        }
        const expense: Expense = new Expense(result.rows[0].expense_id, result.rows[0].reason, result.rows[0].amount,result.rows[0].photo,result.rows[0].wedding_id_fk)
        return expense;
    }
    async updateExpense(expense: Expense): Promise<Expense>
    {
        const sql:string = 'update expense set reason = $1, amount =$2, photo=$3 ,wedding_id_fk = $4 where expense_id = $5';
        const values = [expense.reason,expense.amount,expense.photo,expense.wedding_id_fk,expense.expense_id];
        const result = await connection.query(sql,values);
        return expense;
    }
    async deleteExpenseByID(expense_id: number): Promise<boolean> 
    {
        const sql: string = 'delete from expense where expense_id =$1'
        const values = [expense_id];
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Expense with id ${expense_id} does not exist`);
        }
        return true;
    }

    async getAllExpenses(): Promise<Expense[]>
    {
        const sql:string = "select * from expense";
        const result = await connection.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows)
        {
            const expense:Expense = new Expense(row.expense_id, row.reason, row.amount, row.photo,row.wedding_id_fk);
            expenses.push(expense);
        }
        if(expenses.length == 0)
        {
            throw new MissingResourceError('There are no expenses')
        }
        return expenses;
    }
    
}