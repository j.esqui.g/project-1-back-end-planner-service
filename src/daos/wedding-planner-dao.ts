import { Wedding } from "../entities";
import { Expense } from "../entities";

export interface WeddingPlannerDAO
{
    ////////////////////// WEDDING

    // CREATE
    createWedding(wedding:Wedding): Promise<Wedding>
    // READ
    getAllWeddings(): Promise<Wedding[]>
    getWeddingByID(wedding_id:number): Promise<Wedding>
    // UPDATE
    updateWedding(wedding:Wedding): Promise<Wedding>;
    // DELETE
    deleteWeddingByID(wedding_id:number): Promise<boolean>

    ////////////////////// EXPENSE

    // CREATE
    createExpense(expense:Expense): Promise<Expense>;
    // READ
    getExpenseByID(wedding_id_fk:number): Promise<Expense[]>;
    getExpense(expense_id:number): Promise<Expense>;
    getAllExpenses():Promise<Expense[]>
    // UPDATE
    updateExpense(expense:Expense): Promise<Expense>;
    // DELETE
    deleteExpenseByID(expense_id:number): Promise<boolean>
}