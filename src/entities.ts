
export class Wedding{

    constructor(
        public wedding_id:number,
        public wedding_date:string,
        public wedding_location:string,
        public wedding_name:string,
        public wedding_budget:number
        )
    {}
}

export class Expense{

    constructor(
        public expense_id:number,
        public reason:string,
        public amount:number,
        public photo:string,
        public wedding_id_fk:number
        )
    {}
}