import { Wedding } from "../entities";
import { Expense } from "../entities";

export default interface WeddingPlannerService{

    registerWedding(wedding:Wedding): Promise<Wedding>;

    retrieveAllWeddings(): Promise<Wedding[]>;

    retrieveWeddingByID(wedding_id:number): Promise<Wedding>;

    modifyWedding(wedding: Wedding): Promise<Wedding>;

    removeWeddingByID(wedding_id:number): Promise<boolean>;

    registerExpense(expense:Expense): Promise<Expense>;

    getExpensesByID(wedding_id_fk:number): Promise<Expense[]>;

    getExpenses(expense_id:number):Promise<Expense>

    updateExpense(expense:Expense):Promise<Expense>;

    //depositOrWithdrawIntoAccount(account:Account):Promise<Account>;
    retrieveAllExpenses():Promise<Expense[]>;

    removeExpenseByID(expense_id:number):Promise<boolean>;


}