import { Wedding } from "../entities";
import { Expense } from "../entities";
import { WeddingPlannerDAO } from "../daos/wedding-planner-dao";
import { WeddingPlannerDaoPostgres } from "../daos/wedding-planner-dao-postgres";
import WeddingPlannerService from "./wedding-planner-services";
import { isThisTypeNode } from "typescript";

export class WeddingPlannerServicesImpl implements WeddingPlannerService
{

    weddingplannerDAO:WeddingPlannerDAO = new WeddingPlannerDaoPostgres();

    retrieveAllWeddings(): Promise<Wedding[]>
    {
        return this.weddingplannerDAO.getAllWeddings();
    }
    retrieveWeddingByID(wedding_id: number): Promise<Wedding> 
    {
        return this.weddingplannerDAO.getWeddingByID(wedding_id);
    }
    registerWedding(wedding: Wedding): Promise<Wedding>
    {
        return this.weddingplannerDAO.createWedding(wedding)
    }
    modifyWedding(wedding: Wedding): Promise<Wedding> 
    {
        return this.weddingplannerDAO.updateWedding(wedding);
    }
    removeWeddingByID(wedding_id: number): Promise<boolean> 
    {
        return this.weddingplannerDAO.deleteWeddingByID(wedding_id)
    }
    registerExpense(expense: Expense): Promise<Expense> 
    {
        return this.weddingplannerDAO.createExpense(expense)
    }
    getExpensesByID(wedding_id_fk: number): Promise<Expense[]> 
    {
        return this.weddingplannerDAO.getExpenseByID(wedding_id_fk)
    }
    getExpenses(expense_id: number): Promise<Expense> 
    {
        return this.weddingplannerDAO.getExpense(expense_id);
    }
    updateExpense(expense: Expense): Promise<Expense> 
    {
        return this.weddingplannerDAO.updateExpense(expense)
    }
    removeExpenseByID(expense_id: number): Promise<boolean> 
    {
        return this.weddingplannerDAO.deleteExpenseByID(expense_id);
    }
    retrieveAllExpenses(): Promise<Expense[]>
    {
        return this.weddingplannerDAO.getAllExpenses();
    }

    
}